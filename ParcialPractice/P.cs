﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialPractice
{
    public class Tenis
    {
        public int p1 { get; set; }
        public int p2 { get; set; }

        public Tenis()
        {
            p1 = 0;
            p2 = 0;
        }

        public void Punto(string jugador)
        {
            if(jugador== "p1")
                p1++;
            else if(jugador == "p2")
                p2++;
        }

        public string Ganador()
        {
            var diferencia = Math.Abs(p1 - p2);
            if (p1 > 3 && diferencia > 1)
                return "J1";
            if (p2 > 3 && diferencia > 1)
                return "J2";
            return "Todavía";
        }

        [TestFixture]
        class Tenis_1_Ganador
        {
            [Test]
            public void Test1()
            {
                var p = new Tenis_1();
                p.Punto("J1");
                p.Punto("J2");
                p.Punto("J2");
                p.Punto("J2");
                p.Punto("J2");
                Assert.AreEqual("J2", p.GetGanador());
            }
        }

        [TestFixture]
        class Tenis_1_Puntaje
        {
            [Test]
            public void Test1()
            {
                var p = new Tenis_1();
                p.Punto("J1");
                p.Punto("J1");
                Assert.AreEqual("15-15", p.GetPuntaje());
            }
        }
        
}
