﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialPractice
{
    class CodeFights
    {
        int matrixElementsSum_8(int[][] matrix)
        {
            var sum = 0;
            var actual = 0;
            var cadena = "";
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    actual = matrix[i][j];
                    if (((i > 0 && matrix[i - 1][j] != 0) || i == 0) && (actual != 0))
                    {
                        sum += actual;
                        cadena = actual.ToString();
                    }
                    else
                    {
                        matrix[i][j] = 0;
                        cadena = "x";
                    }
                    Console.Write(cadena + ",");
                }
                Console.Write("\n");
            }
            return sum;
        }

        string[] allLongestStrings_9(string[] inputArray)
        {
            var maxL = 0;
            List<string> inputArray2 = new List<string>();
            foreach (var s in inputArray)
            {
                if (s.Length > maxL)
                    maxL = s.Length;
            }
            foreach (var s in inputArray)
            {
                if (s.Length == maxL)
                    inputArray2.Add(s);
            }
            return inputArray2.ToArray();
        }

        int commonCharacterCount_10(string s1, string s2)
        {
            var cc = 0;
            for (int k = 0; k < s1.Length; k++)
            {
                var c = s1[k];
                var i = 0;
                for (int j = 0; j < s2.Length; j++)
                {
                    var c2 = s2[j];
                    if (c == c2)
                    {
                        //Console.WriteLine(c);
                        cc++;
                        var aStringBuilder = new StringBuilder(s2);
                        aStringBuilder.Remove(j, 1);
                        aStringBuilder.Insert(j, " ");
                        s2 = aStringBuilder.ToString();
                        //Console.WriteLine(s1+"\n"+s2+"\n");
                        break;
                    }
                    i++;
                }
            }
            return cc;
        }

        public bool isLucky_11(string num)
        {
            var l = num.Length;
            double s1 = 0, s2 = 0;
            if (l % 2 != 0) { 
                return false;
            }
            for (int i = 0; i < l; i++)
            {
                if (i < l / 2)
                {
                    s1 += Char.GetNumericValue(num[i]);
                }
                else
                {
                    s2 += Char.GetNumericValue(num[i]);
                }
            }
            return s1 == s2;
        }

        int[] sortByHeight_12(int[] a)
        {
            List<int> posiciones = new List<int>();
            List<int> alturas = new List<int>();
            //guardar las posiciones y alturas
            for (int i = 0; i < a.Count(); i++)
            {
                if (a[i] > 0)
                {
                    posiciones.Add(i);
                    alturas.Add(a[i]);
                }
            }
            //ordenar alturas
            alturas.Sort();

            //insertar alturas ordenadas en posiciones
            for (int i = 0; i < posiciones.Count(); i++)
            {
                a[posiciones[i]] = alturas[i];
            }
            return a;
        }
    }
}
