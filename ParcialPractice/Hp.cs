﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialPractice
{
    public class Hp
    {
        public float CalcTotal(List<int> books, float total=0)
        {
            List<int> books2 = new List<int>();

            if (books.Count == 0)
                return total;
            else
            {
                /* foreach (var b in books)
                 {
                     if (b == 0)
                         books.Remove(b);
                     else
                         b--;
                 }*/
                foreach (var b in books)
                {
                    if (b > 0)
                        books2.Add(b - 1);
                }
                /*for (int i = 0; i < books.Count; i++)
                {
                    var b = books.ElementAt(i);
                    if (b == 0) { 
                        books.Remove(b);
                        if (i > 0)
                            i--;
                    }
                    else { 
                        b--;
                    }
                }*/

                var c = books2.Count;
                float desc;
                switch (c)
                {
                    case 2:
                        desc = 0.05f;
                        break;
                    case 3:
                        desc = 0.1f;
                        break;
                    case 4:
                        desc = 0.2f;
                        break;
                    case 5:
                        desc = 0.25f;
                        break;
                    default:
                        desc = 0;
                        break;
                }
                total += c * 8 * (1 - desc);
                return CalcTotal(books2,total);
            }
            /*if(books.Count==1 && books.First()==1)
                return 8;*/
        }
    }
}
