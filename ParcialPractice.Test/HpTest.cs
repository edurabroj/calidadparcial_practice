﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialPractice.Test
{
    [TestFixture]
    public class HpTest
    {
        private Hp hp;
                
        [SetUp]
        public void Init()
        {
            hp = new Hp();
        }

        [Test]
        [Category("Un libro")]
        public void T1()
        {
            var r1 = hp.CalcTotal(new List<int> {1});
            Assert.AreEqual(8, r1);
        }

        [Test]
        [Category("Dos libros")]
        public void T3()
        {
            var r1 = hp.CalcTotal(new List<int> { 1,1 });
            Assert.AreEqual(15.2f, r1);
        }

        [Test]
        [Category("Complejo")]
        public void T2()
        {
            var r1 = hp.CalcTotal(new List<int> {2,2,2,1,1});
            Assert.AreEqual(51.6f, r1);
        }

        [Test]
        [Category("Complejo")]
        public void T4()
        {
            var r1 = hp.CalcTotal(new List<int> {1,1,1,1,1,1,1,1,1,1});
            Assert.AreEqual(60, r1);
        }
    }
}
